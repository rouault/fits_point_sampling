#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# Purpose:  Vectorize Euclid binary masks using gdal_polygonize
# Author:   Even Rouault <even dot rouault at spatialys.com>
#
###############################################################################
# Copyright (c) 2020, Centre National d'Etudes Spatiales
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
###############################################################################


"""Python program to vectorize Euclid binary masks using gdal_polygonize (gdal must be compiled with fits support)

    Example:

        python3 fits_point_sampling.py --wcs my.fits polygon point 1000
"""

import argparse
import os
import random
import sys

from osgeo import gdal, ogr
from astropy.wcs import WCS


# this allows GDAL to throw Python Exceptions
gdal.UseExceptions()


def get_argument_parser():
    """ Create and return a parser for the command line arguments """
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument(
        'infile',
        help="Input binary mask (fits file)",
        type=str)
    parser.add_argument(
        'polygon_outfile',
        help="Output polygons file without extension (written in geojson format)",
        type=str)
    parser.add_argument(
        'point_outfile',
        help="Output points file without extension (written in geojson format)",
        type=str)
    parser.add_argument(
        'point_count',
        help="Total number of random points to sample",
        type=int)
    parser.add_argument('--wcs', dest='wcs', action='store_true',
                        help="Reproject geometries using wcs (default)")
    parser.add_argument('--no-wcs', dest='wcs', action='store_false',
                        help="Keep geometries in pixel coordinates")
    parser.set_defaults(wcs=True)

    parser.add_argument('--inverse-mask', dest='inverse_mask', action='store_true',
                        help="Whether to polygonize raster values at 0 (rather than at 1)")

    parser.add_argument('--out-dir', dest='out_dir', type=str,
                        default='.',
                        help="Output directory. If not specified, current directory")

    parser.add_argument('--quiet', dest='quiet',
                        action='store_true', help="To disable verbose output")

    # Mostly for debug purposes. Skip polygonization if already done
    parser.add_argument('--skip-polygonize', dest='skip_polygonize',
                        action='store_true', help=argparse.SUPPRESS)

    return parser


def random_sampling(polygon_layer, point_layer, total_point_count, args):
    """ Iterate over polygon_layer to generate approximatively
        total_point_count points in point_layer """
    total_area = sum(f.GetGeometryRef().GetArea() for f in polygon_layer)

    out_points = 0
    for f in polygon_layer:
        polygon = f.GetGeometryRef()
        polygon_area = polygon.Area()
        point_count = int(round(total_point_count * polygon_area / total_area))
        minx, maxx, miny, maxy = polygon.GetEnvelope()
        for i in range(point_count):

            # Prepared geometry added in GDAL 3.3
            try:
                prepared_geom = polygon.CreatePreparedGeometry()
            except AttributeError:
                prepared_geom = None

            while True:
                x = random.uniform(minx, maxx)
                y = random.uniform(miny, maxy)
                p = ogr.Geometry(ogr.wkbPoint)
                p.SetPoint_2D(0, x, y)
                if prepared_geom:
                    if prepared_geom.Contains(p):
                        break
                else:
                    if p.Within(polygon):
                        break

            f = ogr.Feature(point_layer.GetLayerDefn())
            f.SetGeometry(p)
            point_layer.CreateFeature(f)
            out_points += 1
            if not args.quiet:
                gdal.TermProgress_nocb(float(out_points) /
                                       total_point_count, "", None)
    if not args.quiet:
        gdal.TermProgress_nocb(1.0, "", None)


def reproject_polygon_layer(polygon_layer, wcs_polygon_layer, w):
    """ Iterate over polygon_layer to reproject its polygon geometries using
        WCS object w into wcs_polygon_layer """

    for f in polygon_layer:
        g = f.GetGeometryRef()
        g_out = ogr.Geometry(ogr.wkbPolygon)
        for iring in range(g.GetGeometryCount()):
            ring = g.GetGeometryRef(iring)
            ring_out = ogr.Geometry(ogr.wkbLinearRing)
            for ipoint in range(ring.GetPointCount()):
                x, y = ring.GetPoint_2D(ipoint)
                wx, wy = w.wcs_pix2world(x, y, 1)
                wx, wy = (float(wx), float(wy))
                ring_out.AddPoint_2D(wx, wy)
            g_out.AddGeometry(ring_out)
        f_out = ogr.Feature(wcs_polygon_layer.GetLayerDefn())
        f_out.SetGeometry(g_out)
        wcs_polygon_layer.CreateFeature(f_out)


def reproject_point_layer(point_layer, wcs_point_layer, w):
    """ Iterate over point_layer to reproject its point geometries using
        WCS object w into wcs_point_layer """

    for f in point_layer:
        g = f.GetGeometryRef()
        x, y = g.GetPoint_2D(0)
        wx, wy = w.wcs_pix2world(x, y, 1)
        wx, wy = (float(wx), float(wy))
        g_out = ogr.Geometry(ogr.wkbPoint)
        g_out.SetPoint_2D(0, wx, wy)
        f_out = ogr.Feature(wcs_point_layer.GetLayerDefn())
        f_out.SetGeometry(g_out)
        wcs_point_layer.CreateFeature(f_out)


def make_valid_layer(source_filename, output_filename, out_layername, out_format, args):
    """ Run the Geometry.MakeValid() function to create Single-Feature compliant polygons """
    src_ds = ogr.Open(source_filename)
    src_layer = src_ds.GetLayer(0)
    out_ds = ogr.GetDriverByName(out_format).CreateDataSource(output_filename)
    out_layer = out_ds.CreateLayer(out_layername)
    feature_count = src_layer.GetFeatureCount()
    i = 0
    for src_feature in src_layer:
        out_feature = ogr.Feature(out_layer.GetLayerDefn())
        out_feature.SetGeometry(src_feature.GetGeometryRef().MakeValid())
        out_layer.CreateFeature(out_feature)
        i += 1
        if not args.quiet:
            gdal.TermProgress_nocb(float(i) / feature_count, "", None)
    del out_ds


def main(arguments):

    args = get_argument_parser().parse_args(arguments)

    src_ds = gdal.Open(args.infile)

    if args.inverse_mask:
        # Remap values from 0 to 1 and 1 to 0 in a temporary in-memory VRT file
        src_ds = gdal.Translate('', src_ds, options='-of VRT -scale 0 1 1 0')

    srcband = src_ds.GetRasterBand(1)

    # Write in geojson
    ogr_format = 'geojson'

    # Create polygon dataset
    drv = ogr.GetDriverByName(ogr_format)
    polygon_layername = args.polygon_outfile
    polygon_filename = os.path.join(
        args.out_dir, polygon_layername + '.' + ogr_format)

    if not args.skip_polygonize:
        tmp_polygon_filename = polygon_filename + ".tmp"
        tmp_polygon_ds = drv.CreateDataSource(tmp_polygon_filename)
        tmp_polygon_layer = tmp_polygon_ds.CreateLayer(
            polygon_layername, geom_type=ogr.wkbPolygon)

        # Run polygonization
        if not args.quiet:
            print("Polygonizing in %s..." % polygon_filename)
        progress_cbk = None if args.quiet else gdal.TermProgress
        gdal.Polygonize(srcband, srcband,
                        tmp_polygon_layer, -1, [], progress_cbk)

        if not args.quiet:
            print("Running MakeValid() on %s..." % polygon_filename)
        del tmp_polygon_ds
        # Run the Geometry.MakeValid() function to create Single-Feature compliant polygons
        make_valid_layer(tmp_polygon_filename, polygon_filename,
                         polygon_layername, ogr_format, args)
        os.unlink(tmp_polygon_filename)

    # Re-open to be able to iterate over file
    polygon_ds = ogr.Open(polygon_filename)
    polygon_layer = polygon_ds.GetLayer(0)

    # Create point dataset
    point_layername = args.point_outfile
    point_filename = os.path.join(
        args.out_dir, point_layername + '.' + ogr_format)
    point_ds = drv.CreateDataSource(point_filename)
    point_layer = point_ds.CreateLayer(point_layername, geom_type=ogr.wkbPoint)

    if not args.quiet:
        print('Sampling random points in %s...' % point_filename)
    random_sampling(polygon_layer, point_layer, args.point_count, args)

    # Do optional conversion of pixel coordinates to WCS world coordinates
    if args.wcs:

        w = WCS(args.infile)

        wcs_polygon_filename = os.path.join(
            args.out_dir, polygon_layername + '_wcs' + '.' + ogr_format)
        if not args.quiet:
            print('Convert polygon coordinates using FITS WCS in %s...' %
                  wcs_polygon_filename)

        wcs_polygon_ds = drv.CreateDataSource(wcs_polygon_filename)
        wcs_polygon_layer = wcs_polygon_ds.CreateLayer(
            polygon_layername, geom_type=ogr.wkbPolygon)

        reproject_polygon_layer(polygon_layer, wcs_polygon_layer, w)

        del wcs_polygon_ds

        wcs_point_file = os.path.join(
            args.out_dir, point_layername + '_wcs' + '.' + ogr_format)
        if not args.quiet:
            print('Convert point coordinates using FITS WCS in %s...' %
                  wcs_point_file)

        # Re-open to be able to iterate over file
        del point_ds
        point_ds = ogr.Open(point_filename)
        point_layer = point_ds.GetLayer(0)

        wcs_point_ds = drv.CreateDataSource(wcs_point_file)
        wcs_point_layer = wcs_point_ds.CreateLayer(
            point_layername, geom_type=ogr.wkbPoint)

        reproject_point_layer(point_layer, wcs_point_layer, w)

        del wcs_point_layer


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
