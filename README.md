# Polygonization and point sampling

The fits_point_sampling.py script performs the following tasks:

- read a FITS binary mask and create polygons from it into a GeoJSON file (in pixel coordinates)
- sample random points in the polygons
- optionally convert the coordinates of the polygons and points in WCS world coordinates

## Prerequisites

- GDAL (>= 3.0) library with FITS support (Flexible Image Transport System):
  <https://gdal.org/drivers/raster/fits.html>
- GDAL Python bindings
- Python astropy module

You can build a Docker image with the required dependencies with:

```shell
docker build -t gdal-with-astropy .
```

## Usage

```shell
fits_point_sampling.py [-h] [--wcs] [--no-wcs] [--inverse-mask] [--out-dir OUT_DIR] [--quiet] infile polygon_outfile point_outfile point_count
```

Python program to vectorize Euclid binary masks using gdal_polygonize (gdal must be compiled with fits support)

positional arguments:
  infile             Input binary mask (fits file)
  polygon_outfile    Output polygons file without extension (written in geojson format)
  point_outfile      Output points file without extension (written in geojson format)
  point_count        Total number of random points to sample

optional arguments:
  -h, --help         show this help message and exit
  --wcs              Reproject geometries using wcs (default)
  --no-wcs           Keep geometries in pixel coordinates
  --inverse-mask     Whether to polygonize raster values at 0 (rather than at 1)
  --out-dir OUT_DIR  Output directory. If not specified, current directory
  --quiet            To disable verbose output

By default, polygons are created from values at 1 in the input binary mask.
It is possible to create polygons from values at 0 by using the ``--inverse-mask`` switch

## Example

```shell
docker run --rm -it --user $(id -u):$(id -g) -v $HOME:$HOME gdal-with-astropy \
    python $PWD/fits_point_sampling.py --out-dir $PWD --inverse-mask $PWD/EUC_MER_MOSAIC-VIS-FLAG_EBCEFE_20200423T181057.054203Z_00.00.fits polygon point 100
```

## License

X-MIT. See [COPYING](COPYING)
