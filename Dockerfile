FROM osgeo/gdal:latest

RUN apt-get update -y \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --fix-missing --no-install-recommends \
            python3-astropy \
    && rm -rf /var/lib/apt/lists/*
